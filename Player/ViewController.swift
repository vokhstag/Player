//
//  ViewController.swift
//  Player
//
//  Created by Yunus Abubakarov on 31.12.2021.
//

import UIKit

class ViewController: UIViewController {
    
    var audioService = AudioService()
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var progressSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        audioService.playerProgressChanged = { [weak self] value in
            self?.progressSlider.value = Float(value)
        }
    }

    @IBAction func playTapped(_ sender: Any) {
        audioService.playOrPause()
        if audioService.isPlaying {
            playButton.setTitle("Pause", for: .normal)
        } else {
            playButton.setTitle("Play", for: .normal)
        }
    }
    @IBAction func skipForward(_ sender: Any) {
        audioService.skip(forwards: true)
    }
    @IBAction func skipBackward(_ sender: Any) {
        audioService.skip(forwards: false)
    }
    @IBAction func playPiceOfAudio(_ sender: Any) {
        audioService.play(from: 2, to: 6)
    }
    
}

