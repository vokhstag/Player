//
//  AudioService.swift
//  Player
//
//  Created by Yunus Abubakarov on 31.12.2021.
//

import Foundation
import MediaPlayer
import AVFoundation

class AudioService {
    
    init() {
        setupAudio()
        setupDisplayLink()
    }
    
    // MARK: - Visible Properties
    var playerProgressChanged: ((Double) -> Void)?
    var isPlaying = false
    var isPlayerReady = false
    var playerProgress: Double = 0 {
        willSet {
            playerProgressChanged?(newValue)
        }
    }
    var playerTime: PlayerTime = .zero
    
    // MARK: - Private Properties
    private let engine = AVAudioEngine()
    private let player = AVAudioPlayerNode()
    private let timeEffect = AVAudioUnitTimePitch()
    private var needSetupBackgroundPlayer = true
    
    private var audioFile: AVAudioFile?
    private var audioSampleRate: Double = 0 // кол-во аудио симплов в секунду
    private var audioLengthSeconds: Double = 0
    
    private var needsFileScheduled = true
    
    private var displayLink: CADisplayLink?
    private var currentPosition: AVAudioFramePosition = 0 // текущее положение аудио симпла (можно разделить на audioSampleRate и получить текущее время воспроизведения в секундах)
    private var seekFrame: AVAudioFramePosition = 0
    private var allAudioLengthSamples: AVAudioFramePosition = 0 // аудио симплы
    private var audioLengthSamples: AVAudioFramePosition = 0
    private var currentFrame: AVAudioFramePosition {
      guard
        let lastRenderTime = player.lastRenderTime,
        let playerTime = player.playerTime(forNodeTime: lastRenderTime)
      else {
        return 0
      }
      return playerTime.sampleTime
    }
    
    private func setupAudio() {
        guard let fileURL = Bundle.main.url(forResource: "Intro", withExtension: "mp4") else { return }
        do {
            try AVAudioSession.sharedInstance().setCategory(
                AVAudioSession.Category.playback
            )
            try AVAudioSession.sharedInstance().setActive(true)
        } catch let error {
            print(error)
        }
        do {
            let file = try AVAudioFile(forReading: fileURL)
            let format = file.processingFormat
            
            allAudioLengthSamples = file.length
            audioLengthSamples = allAudioLengthSamples
            audioSampleRate = format.sampleRate
            audioLengthSeconds = Double(allAudioLengthSamples) / audioSampleRate
            
            audioFile = file
            configureEngine(with: format)
        } catch {
            print("Error reading the audio file: \(error.localizedDescription)")
        }
    }
    
    private func configureEngine(with format: AVAudioFormat) {
        engine.attach(player)
        engine.attach(timeEffect)
        
        engine.connect(player, to: timeEffect, format: format)
        engine.connect(timeEffect, to: engine.mainMixerNode, format: format)
        engine.prepare()
        
        do {
            try engine.start()
            
            scheduleAudioFile()
            isPlayerReady = true
        } catch {
            print("Error starting the player: \(error.localizedDescription)")
        }
    }
    
    private func scheduleAudioFile() {
        guard let file = audioFile, needsFileScheduled else { return }
        needsFileScheduled = false
        seekFrame = 0
        
        player.scheduleFile(file, at: nil) {
            self.needsFileScheduled = true
        }
    }
    
    func playOrPause() {
        isPlaying.toggle()
        audioLengthSamples = allAudioLengthSamples
        if player.isPlaying {
            displayLink?.isPaused = true
            
            player.pause()
            engine.pause()
        } else {
            try? engine.start()
            displayLink?.isPaused = false

            if needsFileScheduled {
                scheduleAudioFile()
            }
            player.play()
        }
        setupBackgroudPlayerIfNeeded()
        updateNowPlaying()
    }
    
    func skip(forwards: Bool) {
        let timeToSeek: Double
        audioLengthSamples = allAudioLengthSamples
        if forwards {
            timeToSeek = 10
        } else {
            timeToSeek = -10
        }
        seek(to: timeToSeek)
    }
    
    func play(from startTime: Double, to endTime: Double) {
        audioLengthSamples = AVAudioFramePosition(endTime * audioSampleRate)
        audioLengthSamples = min(audioLengthSamples, allAudioLengthSamples)
        go(to: startTime)
        if !isPlaying {
            try? engine.start()
            displayLink?.isPaused = false
            isPlaying = true
            player.play()
        }
        setupBackgroudPlayerIfNeeded()
        updateNowPlaying()
    }
    
    // MARK: - Display updates
    private func setupDisplayLink() {
        displayLink = CADisplayLink(target: self, selector: #selector(updateDisplay))
        displayLink?.preferredFramesPerSecond = 2
        displayLink?.add(to: .current, forMode: .default)
        displayLink?.isPaused = true
    }
    
    @objc func updateDisplay() {
        currentPosition = currentFrame + seekFrame
        currentPosition = max(currentPosition, 0)
        currentPosition = min(currentPosition, allAudioLengthSamples)
        
        if currentPosition >= allAudioLengthSamples {
            player.stop()
            
            seekFrame = 0
            currentPosition = 0
            
            isPlaying = false
            displayLink?.isPaused = true
            engine.stop()
            updateNowPlaying()
        }
        if currentPosition >= audioLengthSamples && isPlaying {
            displayLink?.isPaused = true
            isPlaying = false
            player.pause()
            engine.pause()
            updateNowPlaying()
        }
        
        playerProgress = Double(currentPosition) / Double(allAudioLengthSamples)
        
        let time = Double(currentPosition) / audioSampleRate
        playerTime = PlayerTime(elapsedTime: time, remainingTime: audioLengthSeconds - time)
    }
    
    private func seek(to time: Double) {
        guard let audioFile = audioFile else { return }
        let offset = AVAudioFramePosition(time * audioSampleRate)
        print(offset)
        seekFrame = currentPosition + offset
        seekFrame = max(seekFrame, 0)
        seekFrame = min(seekFrame, allAudioLengthSamples)
        currentPosition = seekFrame
        
        let wasPlaying = player.isPlaying
        player.stop()
        
        if currentPosition < allAudioLengthSamples {
            updateDisplay()
            needsFileScheduled = false
            
            let frameCount = AVAudioFrameCount(allAudioLengthSamples - seekFrame)
            
            player.scheduleSegment(audioFile, startingFrame: seekFrame, frameCount: frameCount, at: nil) {
                self.needsFileScheduled = true
            }
            
            if wasPlaying {
                player.play()
            }
        }
    }
    
    private func go(to starTime: Double) {
        guard let audioFile = audioFile else { return }
        let starTimePosition = AVAudioFramePosition(starTime * audioSampleRate)
        seekFrame = starTimePosition
        seekFrame = max(starTimePosition, 0)
        seekFrame = min(starTimePosition, audioLengthSamples)
        print(starTimePosition)
        currentPosition = seekFrame
        isPlaying = false
        player.stop()
        if currentPosition < audioLengthSamples {
            updateDisplay()
            needsFileScheduled = false
            let frameCount = AVAudioFrameCount(allAudioLengthSamples - seekFrame)
            player.scheduleSegment(audioFile, startingFrame: seekFrame, frameCount: frameCount, at: nil) {
                self.needsFileScheduled = true
            }
        }
    }
    
    private func setupBackgroudPlayerIfNeeded() {
        if needSetupBackgroundPlayer {
            setupRemoteTransportControls()
            setupNowPlaying()
            needSetupBackgroundPlayer = false
        }
    }
    
    func setupRemoteTransportControls() {
        // Get the shared MPRemoteCommandCenter
        let commandCenter = MPRemoteCommandCenter.shared()
    
        // Add handler for Play Command
        commandCenter.playCommand.addTarget { [weak self] event in
            guard let self = self else { return .commandFailed }
            if !self.isPlaying {
                try? self.engine.start()
                if self.needsFileScheduled {
                    self.scheduleAudioFile()
                }
                self.audioLengthSamples = self.allAudioLengthSamples
                self.displayLink?.isPaused = false
                self.isPlaying = true
                self.updateNowPlaying()
                self.player.play()
                return .success
            }
            return .commandFailed
        }
        commandCenter.pauseCommand.addTarget { [weak self] event in
            guard let self = self else { return .commandFailed }
            if self.isPlaying {
                self.engine.pause()
                self.displayLink?.isPaused = true
                self.isPlaying = false
                self.player.pause()
                return .success
            }
                return .commandFailed
            }
        }
    
    func setupNowPlaying() {
        // Define Now Playing Info
        var nowPlayingInfo = [String : Any]()
        nowPlayingInfo[MPMediaItemPropertyTitle] = "Audio"
        
        if let image = UIImage(named: "Bayna") {
            nowPlayingInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(boundsSize: image.size) { size in
                return image
            }
        }
        nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = (Double(currentPosition) / audioSampleRate)
        nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = audioLengthSeconds
        nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = player.rate
        
        // Set the metadata
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }
    
    func updateNowPlaying() {
        // Define Now Playing Info
        var nowPlayingInfo = MPNowPlayingInfoCenter.default().nowPlayingInfo!
        
        nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = (currentPosition) / Int64(audioSampleRate)
        nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = isPlaying ? 1 : 0
        // Set the metadata
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }
}



//// ДОБАВЛЕНИЯ ДЛЯ КОНТРОЛЯ ПРОИГРЫВАНИЯ В ФОНОВОМ РЕЖИМЕ
//func setupRemoteTransportControls() {
//    // Get the shared MPRemoteCommandCenter
//    let commandCenter = MPRemoteCommandCenter.shared()
//
//    // Add handler for Play Command
//    commandCenter.playCommand.addTarget { [weak self] event in
//        if self != nil {
//        if !self!.player.isPlaying {
//            self!.play()
//            return .success
//        }
//        }
//        return .commandFailed
//    }
//
//    // Add handler for Pause Command
//    commandCenter.pauseCommand.addTarget { [weak self] event in
//        guard self != nil else { return .commandFailed }
//        if self!.player.isPlaying {
//            self!.pause()
//            return .success
//        }
//        return .commandFailed
//    }
//}
//
//func setupNowPlaying() {
//    // Define Now Playing Info
//    var nowPlayingInfo = [String : Any]()
//    nowPlayingInfo[MPMediaItemPropertyTitle] = viewModel.ruText(indexPath: IndexPath(row: 0, section: 0))
//
//    if let image = UIImage(named: "Bayna") {
//        nowPlayingInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(boundsSize: image.size) { size in
//            return image
//        }
//    }
//    nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = player.currentTime
//    nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = player.duration
//    nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = player.rate
//
//    // Set the metadata
//    MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
//}
//
//func updateNowPlaying(isPause: Bool) {
//    // Define Now Playing Info
//    var nowPlayingInfo = MPNowPlayingInfoCenter.default().nowPlayingInfo!
//
//    nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = player.currentTime
//    nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = isPause ? 0 : 1
//
//    if !isPause {
//        playBotton.setImage(UIImage(imageLiteralResourceName: "pouseIcon"), for: .normal)
//        playBotton.contentEdgeInsets.left = 0   //Выравнивание картинки на кнопке
//        isAudioPlay = true
//    } else {
//        playBotton.setImage(UIImage(imageLiteralResourceName: "playIcon"), for: .normal)
//        playBotton.contentEdgeInsets.left = 5
//        isAudioPlay = false
//        if let indPath = self.tableView.indexPathsForSelectedRows?.first {
//            self.tableView.deselectRow(at: indPath, animated: true)
//        }
//    }
//
//    // Set the metadata
//    MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
//}
//
////    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
////        print("Audio player did finish playing: \(flag)")
////        if (flag) {
////            updateNowPlaying(isPause: true)
////            //playPauseButton.setTitle("Play", for: UIControl.State.normal)
////        }
////    }
//Конец Добавлений
